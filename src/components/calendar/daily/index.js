import React from 'react';
import './../cal.css';

const createData = (cal, member) => {

}

const CalDaily = (props, context) => {
    const team = context.state.team.members;
    const calendar = context.state.calendar;
    const now = (new Date())

    return (<div className="calendar calendar-vertical">
        { team.map(member => (
            <col className="calRow">
                <member>{member.name}</member>
                {createData(calendar, member)}
            </col>
        ))}
    </div>);
};

CalDaily.contextTypes = {
    state: React.PropTypes.object
};

export default CalDaily;