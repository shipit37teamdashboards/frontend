import React from 'react';
import {createColData} from '../util';
import './../cal.css';

const daysBefore = 7;
const daysAfter = 30 - 7;

const CalMon = (props, context) => {
    const {skipname, member} = props;

    const team = (member ? [member] : context.state.team.members) || [];
    const calendar = context.state.calendar;

    return (<div className="calendar calendar-horizontal">

        { team && team.map(member => (
            <row className="calRow" key={`cm-member-${member.username}`}>
                { skipname ? undefined : <member>{member.name}</member>}
                {createColData(calendar, member, [daysBefore, daysAfter])}
            </row>
        ))}
    </div>);
};

CalMon.propTypes = {
    member: React.PropTypes.object,
    skipname: React.PropTypes.bool
};

CalMon.contextTypes = {
    state: React.PropTypes.object
};

export default CalMon;