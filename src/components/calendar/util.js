import React from 'react';

const CalCell = () => {
    const color = Math.random() < 0.7
        ? 'white'
        : (Math.random() < 0.8 ? 'green' : 'red');
    return (<div className={`calCell calCell-${color}`}></div>)
};

CalCell.propTypes = {
    event: React.PropTypes.object
};

const createColData = (calendar, member, [daysBefore, daysAfter]) => {
    //const cal = calendar[member.calId];
    const result = [];
    for (let i = -daysBefore; i < daysAfter; ++i) {
        result.push(<CalCell event={event} key={i}/>);
    }
    return result;
};


export {
    CalCell,
    createColData
}