import React from 'react';
import './index.css';


const ReposList = (props, context) => {
    return (<ul className="aui-nav beehive-repolist">
        <table className="aui">
            <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Address</th>
                <th>About</th>
            </tr>
            </thead>
            <tbody>
            {context.state.repos.map(
                project => (
                    <tr key={project.uuid}>
                        <td><span className="aui-icon aui-icon-small aui-iconfont-bitbucket"/></td>
                        <td>
                            <name>{project.name}</name>
                        </td>
                        <td>
                            <repolink><a href={project.url}>{project.url}</a></repolink>
                        </td>
                        <td>
                            <about>Sharing is a carrying</about>
                        </td>
                    </tr>
                )
            )}
            </tbody>
        </table>


        <div className="aui-flatpack-example">
            <h2>We found a repo, that might be yours...</h2>
            <table className="aui">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>About</th>
                    <th>Is yours?</th>
                </tr>
                </thead>
                <tbody>
                {context.state.newRepos.map(
                    project => (
                        <tr key={project.uuid}>
                            <td><span className="aui-icon aui-icon-small aui-iconfont-bitbucket"/></td>
                            <td>
                                <name>{project.name}</name>
                            </td>
                            <td>
                                <repolink><a href={project.url}>{project.url}</a></repolink>
                            </td>
                            <td>
                                <about>Sharing is a carrying</about>
                            </td>
                            <td>
                                <aui-toggle label="toggle button" id="my-toggle" resolved="" tooltip-on="On"
                                            tooltip-off="Off">
                                    <input type="checkbox" className="aui-toggle-input"
                                           aria-label="toggle button" id="my-toggle-input"
                                           tooltip-on="On" tooltip-off="Off"
                                           original-title=""/>
                                    <span className="aui-toggle-view">
                                    <span
                                        className="aui-toggle-tick aui-icon aui-icon-small aui-iconfont-success"/>
                                    <span
                                        className="aui-toggle-cross aui-icon aui-icon-small aui-iconfont-close-dialog"/>
                                </span>
                                </aui-toggle>
                            </td>
                        </tr>
                    )
                )}
                </tbody>
            </table>
        </div>
    </ul>);
};

ReposList.contextTypes = {
    state: React.PropTypes.object
};

ReposList.propTypes = {};

export default ReposList;