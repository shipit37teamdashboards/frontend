import React from 'react';
import './index.css';

const ServicesList = (props, context) => {
    let i = 0;
    const members = context.state.team.members;
    return (<table className="aui beeservice-list">
            <thead>
            <tr>
                <th width="60"></th>
                <th width="100">Owner</th>
                <th width="150">Name</th>
                <th>Address</th>
                <th>About</th>
            </tr>
            </thead>
            <tbody>
            {context.state.services.map(
                service => {
                    i++;
                    const status = i % 2 === 0
                        ? <span className="aui-lozenge aui-lozenge-success">ONLINE</span>
                        : <span className="aui-lozenge aui-lozenge-error">OFFLINE</span>
                    const mate = members.filter(member=>member.username == service.owner)[0];
                    return (
                        <tr key={service.key}>
                            <td>{status}</td>
                            <td className="teamView">
                            <span className="aui-avatar aui-avatar-project aui-avatar-xsmall">
                                <span className="aui-avatar-inner"
                                      style={{backgroundImage: `url(${mate.photoUrl})`}}></span>
                            </span>
                                &nbsp; {mate.name}
                            </td>
                            <td>
                                <name>{service.name}</name>
                            </td>
                            <td>
                                <repolink>
                                    <a href={service.url}>{service.url}</a><br/>
                                    <a href={service.serviceCentralUrl}>{service.serviceCentralUrl}</a><br/>

                                </repolink>
                            </td>
                            <td>
                                <about>{service.decription}</about>
                            </td>
                        </tr>
                    )
                }
            )}
            </tbody>
        </table>);
};

ServicesList.contextTypes = {
    state: React.PropTypes.object
};

ServicesList.propTypes = {};

export default ServicesList;