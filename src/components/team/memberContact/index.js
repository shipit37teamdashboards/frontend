import React from 'react';
import './index.css';

const contact = (props, context) => {

    const {mate}=props;
    const online = mate.hipchatStatus === "ONLINE" && !name.vacation;

    const mode = online
        ? <span className="aui-lozenge aui-lozenge-success">online</span>
        : <span className="aui-lozenge">offline</span>;


    const more = (<div className="morecontacts">
        {mode}<br/><br/>
        <a href={`hipchat://atlassian.hipchat.com/user/${mate.username}@atlassian.com`}>
            <span className="aui-lozenge aui-lozenge-complete">hipchat</span>
        </a><br /><br />
        <a href={`mail://${mate.username}@atlassian.com`}>
            <span className="aui-lozenge aui-lozenge-current">email</span>
        </a>
    </div>);

    const bage = online
        ? <span className="aui-lozenge aui-lozenge-success">online{more}</span>
        : <span className="aui-lozenge">offline{more}</span>;

    return (<div className="memberContacts">{bage}</div>)
};

contact.propTypes = {
    mate: React.PropTypes.object
};

export default contact;