import React from 'react';

const header = (props, context) => {
    return (<header id="header" role="banner">
        <nav className="aui-header aui-dropdown2-trigger-group" role="navigation" data-aui-responsive="true">
            <div className="aui-header-inner">
                <div className="aui-header-primary">
                    <h1 id="logo" className="aui-header-logo">
                        <a href="/" id="home_link" style={{padding: "0 0px"}}>
                            <span className="aui-header-logo-device">TEAM</span>

                            <div className="header-logo-text-container"><h3 className="header-logo-text">TEAM</h3></div>
                        </a>
                    </h1>
                    <ul className="aui-nav" style={{width: "auto"}}>
                        <li><a href="#">Open company</a></li>
                        <li><a href="#">Open Team</a></li>
                        <li><a href="#">Open People</a></li>
                        <li><a href="#">One Hope</a></li>
                    </ul>
                </div>
                <div className="aui-header-secondary">
                    <ul className="aui-nav">
                        <li>
                            <select onChange={ (event) => {
                                const filterData = context.state.teamList.filter(item => item.name===event.target.value);
                                const id = filterData[0].id;
                                context.API.team.get({id: id})
                            }} value={context.state.team.name}>
                                { context.state && context.state.teamList
                                    .sort((a, b) => a.name > b.name)
                                    .map(team => <option id={team.id} key={team.id}>{team.name}</option>)
                                }
                            </select>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>)
};

header.contextTypes = {
    state: React.PropTypes.object,
    API: React.PropTypes.object
};

export default header;

