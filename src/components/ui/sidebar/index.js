import React from 'react';
import {Link} from 'react-router';

const list = {
    '': <span><span className="aui-icon aui-icon-small aui-iconfont-homepage"/>&nbsp; Overview</span>,
    'team': <span><span className="aui-icon aui-icon-small aui-iconfont-admin-roles"/>&nbsp; Team</span>,
    //'responces': 'Responsibility',
    'calendar': <span><span className="aui-icon aui-icon-small aui-iconfont-teamcals-large"/>&nbsp; Calendar</span>,
    'projects': <span><span className="aui-icon aui-icon-small aui-iconfont-table-copy-row"/>&nbsp; Projects</span>,
    'services': <span><span className="aui-icon aui-icon-small aui-iconfont-version"/>&nbsp; Services</span>,
    'repos': <span><span className="aui-icon aui-icon-small aui-iconfont-sourcetree"/>&nbsp; Repos</span>,
    'rules': <span><span className="aui-icon aui-icon-small aui-iconfont-devtools-branch"/>&nbsp; Rules of Engagement</span>,
};

const sideBar = (props, context) => {
    return (
        <nav className="aui-navgroup aui-navgroup-vertical">
            <div className="aui-navgroup-inner">
                <ul className="aui-nav __skate">
                    { Object.keys(list).map(item => (
                        <li key={item}>
                            <Link to={{name:item}} className="docs-nav-link">{list[item]}</Link>
                        </li>
                    )) }
                </ul>
            </div>
        </nav>
    );
};

sideBar.contextTypes = {
    routeTo: React.PropTypes.func
};

export default sideBar;


