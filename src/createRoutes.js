import React from 'react';
import { Route, IndexRedirect } from 'react-router';

import Main from './pages/main';
import Calendar from './pages/calendar';
import Projects from './pages/projects';
import Repos from './pages/repos';
import Rules from './pages/rules';
import Services from './pages/services';
import Team from './pages/team';



const createRoutes = (baseApp, basepath = '/') => (
    <Route path={basepath} component={baseApp} >
        <IndexRedirect to="overview" />
        <Route name="overview" path="overview" component={Main} />
        <Route name="calendar" path="calendar" component={Calendar} />
        <Route name="projects" path="projects" component={Projects} />
        <Route name="repos" path="repos" component={Repos} />
        <Route name="rules" path="rules" component={Rules} />
        <Route name="services" path="services" component={Services} />
        <Route name="team" path="team" component={Team} />
    </Route>
);

export default createRoutes;
