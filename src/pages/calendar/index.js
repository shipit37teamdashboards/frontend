import React from 'react';
import CalMon from './../../components/calendar/monthly';

const Page = () => {
    return (
        <div>
            <h1>Calendar</h1>
            <CalMon/>
        </div>
    );
};

export default Page;