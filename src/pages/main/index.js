import React from 'react';

import './style.css'

import TeamField from './../../components/fields/team';
import Projects from './../../components/projects/list';
import CalMon from './../../components/calendar/monthly';
import TeamLine from './../../components/team/lineView';
import TeamContacts from './../../components/team/contacts';
import TeamLocation from '../../components/map/teamLocation';


const Page = () => {
    return (
        <div className="aui-page-panel-main">
            <div className="aui-page-panel-inner">
                <section className="aui-page-panel-item aui-page-panel-item-content ">
                    <h1 className="header"><TeamField name="name"/></h1>
                    <h2 className="subheader"><TeamField name="description"/></h2>
                    <div className="--aui-flatpack-example" style={{marginTop: 40, marginBottom: 40}}>
                        <TeamLine/>
                    </div>
                    <div className="aui-flatpack-example">
                        <TeamLocation />
                    </div>
                    <div className="aui-flatpack-example">
                        <CalMon/>
                    </div>
                </section>

                <section className="aui-page-panel-item aui-page-panel-item-sidebar">
                    <nav className="aui-navgroup aui-navgroup-vertical">
                        <div className="aui-nav-heading">
                            <strong>Projects</strong>
                        </div>
                        <Projects />
                    </nav>

                    <nav className="aui-navgroup aui-navgroup-vertical">
                        <div className="aui-nav-heading">
                            <strong>Contacts</strong>
                        </div>
                        <TeamContacts />
                    </nav>
                </section>
            </div>
        </div>
    );
};

export default Page;