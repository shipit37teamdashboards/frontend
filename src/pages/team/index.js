import React from 'react';
import TeamContacts from './../../components/team/contacts';
import CalMon from './../../components/calendar/monthly';
import Contact from './../../components/team/memberContact';

const TeamLine = (props, context) => {

    const members = (context.state.team && context.state.team.members) || [];

    let i = 0;
    const nowDate = new Date();
    return (<div>

        <TeamContacts />

        <table className="aui teamOverview">
            <thead>
            <tr>
                <th width="70"></th>
                <th width="80"></th>
                <th>Name</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {members
                .sort((a, b)=> a.name[0] > b.name[0])
                .map(mate => {
                        let className = 'team-member-normal';
                        let bage = (i++) % 2 === 0
                            ? <span className="aui-lozenge aui-lozenge">OOO</span>
                            : <span className="aui-lozenge aui-lozenge-success">in place</span>;
                        if (Math.random() < 0.2) {
                            bage = <span className="aui-lozenge aui-lozenge-moved">vacation</span>;
                            className = 'team-member-vacation';
                        }
                        mate.vacation = className === 'team-member-vacation';
                        if (Math.random() < 0.2) {
                            bage = <span className="aui-lozenge aui-lozenge-current">ill</span>;
                            className = 'team-member-ill';
                        }

                        const hipchat = <Contact mate={mate}/>;

                        const hireDate = new Date(mate.hireDate);
                        const ageY = nowDate.getFullYear() - hireDate.getFullYear();
                        const ageM = nowDate.getMonth() > hireDate.getMonth()
                            ? 12 - nowDate.getMonth() + hireDate.getMonth()
                            : hireDate.getMonth() - nowDate.getMonth();
                        const age = ageY ? `${ageY} years` : `${ageM} months`;

                        return (<tr key={mate.username} className={className}>
                            <td>
                                {bage}<br/>
                                {hipchat}<br/>
                                <a href={`hipchat://atlassian.hipchat.com/user/${mate.username}@atlassian.com`}>
                                    <span className="aui-lozenge aui-lozenge-complete">write</span>
                                </a>
                            </td>
                            <td className="teamView">
                        <span className="aui-avatar aui-avatar-project aui-avatar-xlarge">
                                <span className="aui-avatar-inner"
                                      style={{backgroundImage: `url(${mate.photoUrl})`}}></span>
                            </span>
                            </td>
                            <td>
                                <nametab>
                                    <name>{mate.name}</name>
                                    <role>{mate.position}</role>
                                    <spec>{mate.specialization}</spec>
                                    <age>{age} on board</age>
                                </nametab>
                            </td>
                            <td>
                                <CalMon member={mate} skipname/>
                            </td>
                        </tr>)
                    }
                )}
            </tbody>
        </table>
    </div>);
};

TeamLine.contextTypes = {
    state: React.PropTypes.object,
    API: React.PropTypes.object
};

const Page = () => {
    return (
        <div>
            <TeamLine />
        </div>
    );
};

export default Page;