import createAPI from 'unity-api';

import team from './resources/teams';
import projects from './resources/projects';

export default createAPI({ team, projects }, [], 'https://shipit-beehives.internal.domain.dev.atlassian.io/', {
    credentials: false
});