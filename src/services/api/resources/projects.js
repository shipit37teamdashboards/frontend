///projects?usernames=akoshelev,isadykov,hfarrant
export default {
  namespace: 'projects',

  methods: {

    getByUsername: ({ username = [] }) => ({ query: { usernames: username.join() } }),

  }
}