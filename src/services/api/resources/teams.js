export default {
  namespace: 'teams',

  methods: {

    get: ({ id }) => ({ path: id }),

  }
}