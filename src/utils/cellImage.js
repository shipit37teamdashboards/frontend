const generateCellImage = (width, height, color='#888') => {
    const element = document.createElement('canvas');
    const canvas = element.getContext('2d');
    element.width = canvas.width = width;
    element.height = canvas.height = height;
    canvas.translate(-0.5, -0.5);
    canvas.moveTo(width/2, 0);
    canvas.lineTo(width/2, height/2);
    canvas.lineTo(0, height/2);
    canvas.lineJoin = 'miter';
    canvas.lineCap = 'butt';
    canvas.lineWidth = 1;
    canvas.strokeStyle = color;
    canvas.stroke();
    return element.toDataURL();
};

const generateCellVerticalImage = (width, color='#888') => {
    const element = document.createElement('canvas');
    const canvas = element.getContext('2d');
    element.width = canvas.width = width;
    element.height = canvas.height = 1;
    canvas.translate(-0.5, -0.5);
    canvas.moveTo(width/2, 0);
    canvas.lineTo(width/2, 1);
//    canvas.lineTo(0, height);
    canvas.lineJoin = 'miter';
    canvas.lineCap = 'butt';
    canvas.lineWidth = 1;
    canvas.strokeStyle = color;
    canvas.stroke();
    return element.toDataURL();
};


export {
    generateCellImage,
    generateCellVerticalImage
};